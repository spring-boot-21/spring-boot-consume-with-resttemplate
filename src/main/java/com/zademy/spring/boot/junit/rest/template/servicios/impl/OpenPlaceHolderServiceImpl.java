package com.zademy.spring.boot.junit.rest.template.servicios.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.zademy.spring.boot.junit.rest.template.modelo.OpenPlaceHolderModel;
import com.zademy.spring.boot.junit.rest.template.servicios.OpenPlaceHolderService;

/**
 * The Class OpenLibraServiceImpl.
 */
@Service("openPlaceHolderService")
public class OpenPlaceHolderServiceImpl implements OpenPlaceHolderService {

	/** The logger. */
	public static final Logger logger = LoggerFactory.getLogger(OpenPlaceHolderServiceImpl.class);

	/** The rest template. */
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Buscar comentarios con exchange.
	 *
	 * @return the response entity
	 */
	@Override
	public ResponseEntity<List<OpenPlaceHolderModel>> buscarComentariosConExchange() {

		logger.info("Inicia Busqueda Comentarios Con Exchange");

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<OpenPlaceHolderModel> entity = new HttpEntity<>(headers);

		ResponseEntity<List<OpenPlaceHolderModel>> response = restTemplate.exchange(
				"https://jsonplaceholder.typicode.com/comments", HttpMethod.GET, entity,
				new ParameterizedTypeReference<List<OpenPlaceHolderModel>>() {
				});

		logger.info("Response: {}", response.getBody());

		return new ResponseEntity<>(response.getBody(), HttpStatus.OK);
	}

	/**
	 * Buscar comentarios con fonr entity.
	 *
	 * @return the response entity
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<OpenPlaceHolderModel> buscarComentariosConForEntity() {

		logger.info("Inicia Busqueda Comentarios Con For Entity");

		restTemplate.getInterceptors().add(new ClientHttpRequestInterceptor() {
			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
					throws IOException {
				request.getHeaders().setContentType(MediaType.APPLICATION_JSON);
				request.getHeaders().setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
				return execution.execute(request, body);
			}
		});

		ResponseEntity<List> response = restTemplate.getForEntity("https://jsonplaceholder.typicode.com/comments",
				List.class);

		logger.info("Response: {}", response.getBody());

		List<OpenPlaceHolderModel> object = response.getBody();

		return object;
	}

	/**
	 * Buscar comentarios con for object.
	 *
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OpenPlaceHolderModel> buscarComentariosConForObject() {

		logger.info("Inicia Busqueda Comentarios Con For Object");

		restTemplate.getInterceptors().add(new ClientHttpRequestInterceptor() {
			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
					throws IOException {
				request.getHeaders().setContentType(MediaType.APPLICATION_JSON);
				request.getHeaders().setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
				return execution.execute(request, body);
			}
		});

		List<OpenPlaceHolderModel> response = restTemplate.getForObject("https://jsonplaceholder.typicode.com/comments",
				List.class);

		logger.info("Response: {}", response);

		List<OpenPlaceHolderModel> object = response;

		return object;
	}

}
