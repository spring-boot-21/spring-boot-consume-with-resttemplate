package com.zademy.spring.boot.junit.rest.template.controladores;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zademy.spring.boot.junit.rest.template.modelo.OpenPlaceHolderModel;
import com.zademy.spring.boot.junit.rest.template.servicios.OpenPlaceHolderService;

/**
 * The Class OpenLibraController.
 */
@RestController
public class OpenPlaceHolderController {

	/** The logger. */
	public static final Logger logger = LoggerFactory.getLogger(OpenPlaceHolderController.class);

	/** The open place holder service. */
	@Autowired
	@Qualifier("openPlaceHolderService")
	private OpenPlaceHolderService openPlaceHolderService;

	
	/**
	 * Buscar comentarios con exchange.
	 *
	 * @return the response entity
	 */
	@GetMapping(path = "/buscarComentariosConExchange", 
			produces = { MediaType.APPLICATION_JSON_VALUE,
						 MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<OpenPlaceHolderModel>> buscarComentariosConExchange() {

		logger.info("Ingresa a metodo buscarComentariosConExchange");
		
		return openPlaceHolderService.buscarComentariosConExchange();
	}

	
	/**
	 * Buscar comentarios con for entity.
	 *
	 * @return the response entity
	 */
	@GetMapping(path="/buscarComentariosConForEntity", 
			produces = { MediaType.APPLICATION_JSON_VALUE,
					 	 MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<OpenPlaceHolderModel>> buscarComentariosConForEntity() {
		
		logger.info("Ingresa a metodo buscarComentariosConForEntity");

		return new ResponseEntity<>(openPlaceHolderService.buscarComentariosConForEntity(), HttpStatus.OK);
	}
	
	/**
	 * Buscar comentarios con for object.
	 *
	 * @return the response entity
	 */
	@GetMapping(path="/buscarComentariosConForObject", 
			produces = { MediaType.APPLICATION_JSON_VALUE,
					 	 MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<OpenPlaceHolderModel>> buscarComentariosConForObject() {
		
		logger.info("Ingresa a metodo buscarComentariosConForObject");

		return new ResponseEntity<>(openPlaceHolderService.buscarComentariosConForObject(), HttpStatus.OK);
	}

}
