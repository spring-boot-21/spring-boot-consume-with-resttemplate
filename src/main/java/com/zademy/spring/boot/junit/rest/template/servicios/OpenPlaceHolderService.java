package com.zademy.spring.boot.junit.rest.template.servicios;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.zademy.spring.boot.junit.rest.template.modelo.OpenPlaceHolderModel;

/**
 * The Interface OpenPlaceHolderService.
 */
public interface OpenPlaceHolderService {

	/**
	 * Buscar comentarios con exchange.
	 *
	 * @return the response entity
	 */
	ResponseEntity<List<OpenPlaceHolderModel>> buscarComentariosConExchange();

	/**
	 * Buscar comentarios con fonr entity.
	 *
	 * @return the response entity
	 */
	List<OpenPlaceHolderModel> buscarComentariosConForEntity();

	/**
	 * Buscar comentarios con for object.
	 *
	 * @return the list
	 */
	List<OpenPlaceHolderModel> buscarComentariosConForObject();

}
