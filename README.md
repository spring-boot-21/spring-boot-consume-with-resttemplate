## Pre-requisitos
- Spring Boot 2.4.2.RELEASE
- Spring 5.3.3
- Tomcat embed 9.0.41
- Gradle 6.8
- Java 8

## Construir proyecto
```
gradle build
```

## Ejecutar proyecto
```
gradle run
```

Accede a la página utilizando las URL:
- http://localhost:8080/{contexto}/buscarComentariosConExchange
- http://localhost:8080/{contexto}/buscarComentariosConForEntity
- http://localhost:8080/{contexto}/buscarComentariosConForObject


